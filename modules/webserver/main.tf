resource "aws_security_group" "app-sg" {
  vpc_id = var.vpc_id
  name = "app-sg"

  ingress = [{
    cidr_blocks      = [var.my_ip]
    description      = "SSH Rule"
    from_port        = 22
    ipv6_cidr_blocks = []
    prefix_list_ids  = []
    protocol         = "tcp"
    security_groups  = []
    self             = false
    to_port          = 22
    },
    {
      cidr_blocks      = ["0.0.0.0/0"]
      description      = "HTTP Rule"
      from_port        = 8080
      ipv6_cidr_blocks = []
      prefix_list_ids  = []
      protocol         = "tcp"
      security_groups  = []
      self             = false
      to_port          = 8080
  }]

  egress = [{
    cidr_blocks      = ["0.0.0.0/0"]
    description      = "Egress Rule"
    from_port        = 0
    ipv6_cidr_blocks = []
    prefix_list_ids  = []
    protocol         = "-1"
    security_groups  = []
    self             = false
    to_port          = 0
  }]

  tags = {
    "Name" = "${var.env_prefix}-sg"
  }
}

data "aws_ami" "latest-amazon-linux-image" {
  most_recent = true
  owners      = ["amazon"]
  filter {
    name   = "name"
    values = [var.image_name]
  }
  filter {
    name   = "virtualization-type"
    values = ["hvm"]
  }
}

resource "aws_key_pair" "ssh-key" {
  key_name = "${var.env_prefix}-server-ssh-key"
  public_key = file(var.public_key_location)
}

resource "aws_instance" "app-server" {
  ami           = data.aws_ami.latest-amazon-linux-image.id
  instance_type = var.instance_type

  subnet_id              = var.subnet_id
  vpc_security_group_ids = [aws_security_group.app-sg.id]
  availability_zone      = var.avail_zone

  associate_public_ip_address = true
  key_name                    = aws_key_pair.ssh-key.key_name

  user_data = file("entry-script.sh")

#   connection {
#     type = "ssh"
#     host = self.public_ip
#     user = "ec2-user"
#     private_key = file(var.private_key_location)
#   }

#   provisioner "file" {
#     source = "entry-script.sh"
#     destination = "/home/ec2-user/entry-script.sh"
#   }

#   provisioner "remote-exec" {
#     script = file("entry-script.sh")
#   }

#   provisioner "local-exec" {
#     command = "echo ${self.public_ip} > output.txt"
#   }

  tags = {
    "Name" = "${var.env_prefix}-server"
  }
}
